
public class BankAccount {
	private Double balance = 0.0;

	public /*synchronized */void subFromAccount(double howMuch) {
		synchronized (this) {
			balance -= howMuch;
		}
	}

	public /*synchronized */void addToAccount(double howMuch) {
		synchronized (this) {
			balance += howMuch;
		}
	}

	public void balance() {
		System.out.println("Aktualnie na koncie jest : " + balance);
	}
}
