import java.util.Random;

public class BankTransaction implements Runnable {

	private TransactionType type;
	private double amount;
	private BankAccount account;

	public BankTransaction(BankAccount account, TransactionType type, double amount) {
		super();
		this.account = account;
		this.type = type;
		this.amount = amount;
	}

	static int time = 10;
	@Override
	public void run() {
//		long sleepTime = new Random().nextInt(19000) + 1000;
		long sleepTime = time;
		try {
			switch (type) {
			case Balance:
				account.balance();
				break;
			case Add:
				Thread.sleep(sleepTime);
				account.addToAccount(amount);
				System.out.println("Added " + amount);
				break;
			case Sub:
				Thread.sleep(sleepTime);
				account.subFromAccount(amount);
				System.out.println("Subbed " + amount);
				break;
			}
		} catch (InterruptedException e) {
			// TODO: handle exception
		}
	}
}
